import { useState } from "react";

function AddManufacturer() {
    const [name, setName] = useState('')
    const [submitted, setSubmitted] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault()

        const postUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify({'name': name}),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const postRes = await fetch(postUrl, fetchConfig)
        if (postRes.ok) {
            setName('')
            setSubmitted(true)
        } else {
            console.error("Problem posting manufacturer to inventory-api line 17")
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleAddAnother = () => {
        window.location.reload()
    }

    let successMessageClasses = "alert alert-success d-none mb-0"
    let formClasses = ""
    if (submitted === true) {
        successMessageClasses = "alert alert-success mb-0"
        formClasses = "d-none"
    }

    return (
        <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} className={formClasses}>
                        <div className="form-floating mb-3">
                            <input name="name" placeholder="Name" onChange={handleNameChange} required type="text" id="name" value={name} className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={successMessageClasses} id="success-message">
                        <p>Manufacturer added!</p>
                        <button className="btn btn-success" onClick={handleAddAnother}>Add another?</button>
                    </div>
                </div>
            </div>
    )
}
export default AddManufacturer;
