import { useEffect, useState } from "react";

function AddModel() {
    const [makes, setMakes] = useState([])
    const [name, setName] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [make, setMake] = useState('')
    const [submitted, setSubmitted] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.name = name
        data.picture_url = pictureUrl
        data.manufacturer_id = make

        const postUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'appilcation/json'
            }
        }
        const postRes = await fetch(postUrl, fetchConfig)
        if (postRes.ok) {
            setMake('')
            setName('')
            setPictureUrl('')
            setSubmitted(true)
        } else {
            console.error("Problem posting new model to inventory-api line 25")
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleMakeChange = (event) => {
        const value = event.target.value
        setMake(value)
    }

    const handleAddAnother = () => {
        window.location.reload()
    }

    const fetchData = async () => {
        const makesUrl = 'http://localhost:8100/api/manufacturers/'
        const makesRes = await fetch(makesUrl)
        if (makesRes.ok) {
            const makesData = await makesRes.json()
            setMakes(makesData.manufacturers)
        } else {
            console.error("Error getting data from inventory-api line 50")
        }

    }
    useEffect(() => {
        fetchData()
    }, [])

    let successMessageClasses = "alert alert-success d-none mb-0"
    let formClasses = ""
    if (submitted === true) {
        successMessageClasses = "alert alert-success mb-0"
        formClasses = "d-none"
    }

    return (
        <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a Model</h1>
                    <form onSubmit={handleSubmit} className={formClasses}>
                        <div className="mb-3">
                            <select onChange={handleMakeChange} name="make" required id="make" value={make} className="form-select">
                                <option value="">Choose a Make</option>
                                {makes&&makes.map(make => {
                                    return (
                                        <option key={make.id} value={make.id}>
                                            {make.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="name" placeholder="name" onChange={handleNameChange} required type="text" id="name" value={name} className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="pictureUrl" placeholder="Picture Url" onChange={handlePictureUrlChange} required type="url" id="pictureUrl" value={pictureUrl} className="form-control" />
                            <label htmlFor="pictureUrl">Picture Url</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={successMessageClasses} id="successMessage">
                        <p>Model added!</p>
                        <button onClick={handleAddAnother} className="btn btn-success">Add another?</button>
                    </div>
                </div>
            </div>
    )
}
export default AddModel;
