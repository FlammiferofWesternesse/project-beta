import { useEffect, useState } from "react";

function ListManufacturers() {
    const [makes, setMakes] = useState()

    const handleDelete = async (event) => {
        event.preventDefault()

        const makeId = event.target.id
        const deleteUrl = `http://localhost:8100/api/manufacturers/${makeId}`
        const fetchConfig  = {
            method: 'delete'
        }
        const deleteRes = await fetch(deleteUrl, fetchConfig)
        if (deleteRes.ok) {
            window.location.reload()
        } else {
            console.error("Problem deleteing manufacturer in inventory-api line 14")
        }
    }

    const fetchData = async () => {
        const makesUrl = "http://localhost:8100/api/manufacturers/"
        const makesRes = await fetch(makesUrl)
        if (makesRes.ok) {
            const makesData = await makesRes.json()
            setMakes(makesData.manufacturers)
        } else {
            console.error("Error fetching manufacturers list from inventory-api line 24")
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="shadow m-5">
            <table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">Manufacturer Name</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {makes&&makes.map(make => {
                        return (
                        <tr key={make.id} className="align-middle">
                            <td>{make.name}</td>
                            <td onClick={handleDelete} id={make.id} className="btn-md btn-danger text-danger text-center" style={{cursor: "pointer", 'width': 70 + 'px'}}>Delete</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ListManufacturers;
