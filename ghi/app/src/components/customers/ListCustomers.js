import { useEffect, useState } from "react"

function ListCustomers() {
    const [customers, setCustomers] = useState([])

    const handleDelete = async (event) => {
        event.preventDefault()

        const id = event.target.id
        const customersUrl = `http://localhost:8070/api/customers/${id}/`
        const fetchConfig = {
            method: 'delete'
        }
        const deleteResponse = await fetch(customersUrl, fetchConfig)
        if (deleteResponse.ok) {
            window.location.reload()
        } else {
            console.error("Problem deleting customers in sales-api line 14")
        }
    }


    const fetchData = async () => {
        const customerResponse = await fetch('http://localhost:8070/api/customers/')
        if (customerResponse.ok) {
            const customerData = await customerResponse.json()
            setCustomers(customerData.customers)
        } else {
            console.error("Error fetching customers")
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className="shadow m-5">
            <table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {customers&&customers.map(cust => {
                        return (
                        <tr key={cust.id}>
                            <td>{cust.first_name} {cust.last_name}</td>
                            <td>{cust.address}</td>
                            <td>{cust.phone_number}</td>
                            <td onClick={handleDelete} style={{cursor: "pointer"}} id={cust.id} className="btn-md btn-danger text-danger">Delete</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )

};
export default ListCustomers;
