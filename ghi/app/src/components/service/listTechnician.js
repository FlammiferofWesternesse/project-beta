function TechList(props){

return (
<div className="container my-5">
        <table className="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
        </thead>
        <tbody>
            {props.technicians&&props.technicians.map((tech)=>{
                return(
                    <tr className="tabel-info" key={tech.employee_id} value={tech.employee_id}>
                        <td>{tech.first_name}</td>
                        <td>{tech.last_name}</td>
                        <td>{tech.employee_id}</td>
                    </tr>
                )
            })}
        </tbody>
        </table>
    </div>
)
        }
        export default TechList