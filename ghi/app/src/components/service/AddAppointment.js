import React, {useEffect, useState} from 'react'

function AddAppointment() {
    const [submitted, setSubmitted] = useState(false)
    const [date, setDate] = useState('')
    const [reason, setReason] = useState('')
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [tech, setTech] = useState('')
    const [customers, setCustomers] = useState([])
    const [technicians, setTechnicians] = useState([])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.reason = reason;
        data.date_time = date;
        data.vin = vin;
        data.customer = customer;
        data.technician = tech;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if(response.ok){

            setReason('');
            setDate('');
            setVin('');
            setVin('');
            setCustomer('');
            setTech('');
            setSubmitted(true)
        }
    }

    const handleDateChange = (event) => {
        const value=event.target.value
        setDate(value)
    }

    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }


    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleCustChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleTechChange = (event) => {
        const value = event.target.value;
        setTech(value)
    }

    const handleAddAnother = () => {
        window.location.reload()
    }

    const fetchData = async () => {
        const customerResponse = await fetch('http://localhost:8070/api/customers/')
        const techResponse = await fetch('http://localhost:8080/api/technicians/')
        if (
            customerResponse.ok &&
            techResponse
        ) {
            const customerData = await customerResponse.json()
            const techData = await techResponse.json()
            setCustomers(customerData.customers)
            setTechnicians(techData.technicians)
        } else {
            console.error("Error getting data from backend")
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    let successMessageClasses = "alert alert-success d-none mb-0"
    let formClasses = ""
    if (submitted === true) {
        successMessageClasses = "alert alert-success mb-0"
        formClasses = "d-none"
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new appointment</h1>
                <form onSubmit={handleSubmit} className={formClasses} id="create-appointment-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleDateChange} placeholder="date/time" required type="datetime-local" name="date/time" id="date/time" className="form-control" value={date} />
                    <label htmlFor="date/time">Date/time</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleReasonChange} placeholder = "Reason" required type="text" name="reason" id="reason" className="form-control" value={reason} />
                    <label htmlFor="reason">Reason</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin"className="form-control" value={vin} />
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleCustChange} required id="customer" name="customer" className="form-select" >
                        <option value="">Choose a Customer</option>
                        {customers&&customers.map(cust => {
                            return (
                                <option key={cust.id} value={cust.id}>
                                    {cust.first_name} {cust.last_name}
                                </option>
                                );
                            })}
                    </select>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleTechChange} required id="technician" name="technician" className="form-select" >
                        <option value="">Choose a Technician</option>
                        {technicians&&technicians.map(tech => {
                            return (
                                <option key={tech.employee_id} value={tech.employee_id}>
                                    {tech.first_name} {tech.last_name}
                                </option>
                                );
                            })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                <div className={successMessageClasses} id="success-message">
                    Appointment scheduled!
                    <button onClick={handleAddAnother} className='btn btn-success'>Add another?</button>
                </div>
            </div>
        </div>
        </div>
    )
}
export default AddAppointment;
