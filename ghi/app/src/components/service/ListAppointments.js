import React, {useEffect, useState,} from "react";

function GetDateTime(props) {
    const date = props.datetime
    const timestamp = new Date(date)
    const dateString = `${timestamp.getMonth()}/${timestamp.getDate()}/${timestamp.getFullYear()}\n@ ${timestamp.toLocaleTimeString().slice(0, 5)}`
    return dateString
}

function ListAppointments() {
    const [appointments, setAppointments] = useState([])

    const handleCancel = async (param) => {
        const id = param.id
        const putUrl = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({"status": "canceled"}),
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(
            putUrl,
            fetchConfig,
        );

        if (response.ok) {
        } else {
            console.error("Error canceling appointment");
        }
    };

    const handleFinish = async (param) => {
        const id = param.id
        const putUrl = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({"status": "finished"}),
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(
            putUrl,
            fetchConfig,
        );
        if (response.ok){
            window.location.reload()
        } else {
        console.error("error finishing appointment")
        }
    };

const fetchData = async () => {
    const apptResponse = await fetch('http://localhost:8080/api/appointments')
    if (apptResponse.ok) {
        const apptData = await apptResponse.json()
        setAppointments(apptData.appointments.filter(appt => appt.status == "created"))
    }
}

useEffect(() => {
    fetchData();
}, []);

return(
    <div className="shadow m-5">
        <table className="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th scope="col">Date/time</th>
                    <th scope="col"> Reason</th>
                    <th scope="col">Status</th>
                    <th scope="col">VIN</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Technician</th>
                    <th scope="col">VIP</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {appointments&&appointments.map(appt => {
                    return(
                        <tr key={appt.id} value={appt.id}>
                            <td><GetDateTime datetime={appt.date_time}/></td>
                            <td>{appt.reason}</td>
                            <td>{appt.status}</td>
                            <td>{appt.vin.slice(0, 4)}</td>
                            <td>{appt.customer.first_name} {appt.customer.last_name}</td>
                            <td>{appt.technician.first_name}</td>
                            <td>{appt.is_vip ? 'VIP' : 'Regular'}</td>
                            <td>
                                <button onClick={() => handleCancel(appt)} style={{width: 70 + 'px', cursor: "pointer"}} className="btn-md btn-danger danger-border">Cancel</button>
                                <button onClick={() => handleFinish(appt)} style={{width: 70 + 'px', cursor: "pointer"}} className="btn-md btn-success success-border">Finish</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
)
}
export default ListAppointments;
