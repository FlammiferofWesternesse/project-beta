import { NavLink } from "react-router-dom";

function SalesHome() {
    return (
    <div>
        <h1>Welcome to the CarCar Sales Division!</h1>
        <p className="text-center">
            Please select from the following:
        </p>
        <div>
            <ul>
                <li><NavLink to="list" >Sales List</NavLink></li>
                <li><NavLink to="new">New Sale</NavLink></li>
                <li><NavLink to="/customers">Customers</NavLink></li>
                <li><NavLink to="/customers/new">New Customer</NavLink></li>
                <li><NavLink to="/salespeople" >Salespeople</NavLink></li>
                <li><NavLink to="/salespeople/new" >New Saleaperson</NavLink></li>
                <li><NavLink to="/salespeople/history">Salespeople Records</NavLink></li>
            </ul>
        </div>
    </div>
    )
};

export default SalesHome;
