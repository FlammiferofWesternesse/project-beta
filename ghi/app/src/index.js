import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './components/App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

const loadApp = async () => {
  const apptResponse = await fetch('http://localhost:8080/api/appointments');
  const techResponse = await fetch('http://localhost:8080/api/technicians/');
  const customerResponse = await fetch('http://localhost:8070/api/customers/');
  const saleResponse = await fetch('http://localhost:8090/api/sales/');
  const salespersonResponse = await fetch('http://localhost:8090/api/salespeople/');
  const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
  const modelReponse = await fetch('http://localhost:8100/api/models/')
  const makeResponse = await fetch('http://localhost:8100/api/manufacturers/')

  if (
    apptResponse.ok &&
    techResponse.ok &&
    customerResponse.ok &&
    saleResponse.ok &&
    salespersonResponse.ok &&
    automobileResponse.ok &&
    modelReponse.ok &&
    makeResponse.ok
    ) {
    const apptData = await apptResponse.json();
    const techData = await techResponse.json();
    const customerData = await customerResponse.json();
    const saleData = await saleResponse.json();
    const salespeopleData = await salespersonResponse.json();
    const automobileData = await automobileResponse.json();
    const modelData = await modelReponse.json();
    const makeData = await makeResponse.json();

    root.render(
      <React.StrictMode>
        <App
        appointments={apptData.appointments}
        technicians={techData.technicians}
        customers={customerData.customers}
        sales={saleData.sales}
        salespeople={salespeopleData.salespeople}
        automobiles={automobileData.autos}
        models={modelData.models}
        manufacturers={makeData.manufacturers}
        />
      </React.StrictMode>
    )
  } else {
    console.error("Failure to fetch one or more resouces")
  }
}
loadApp()
