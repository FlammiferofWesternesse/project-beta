import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Customer
from .encoders import CustomerEncoder


# Create your views here.
@require_http_methods(["GET", "POST"])
def show_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Exception:
            return JsonResponse(
                {"message": "Failure to post customer object"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def show_customer_details(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Customer ID"},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            Customer.objects.filter(id=pk).update(**content)
        except Exception:
            return JsonResponse(
                {"message": "Error updating customer data"},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=pk)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=404,
            )
        return JsonResponse()
    else:
        deleted, _ = customer.delete()
        return JsonResponse(
            {"deleted": deleted > 0},
        )
