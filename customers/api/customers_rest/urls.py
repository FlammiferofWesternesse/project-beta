from django.urls import path
from .views import (
    show_customer_list,
    show_customer_details,
)


urlpatterns = [
    path("", show_customer_list, name="show_customer_list"),
    path("<int:pk>/", show_customer_details, name="show_customer_details")
]
