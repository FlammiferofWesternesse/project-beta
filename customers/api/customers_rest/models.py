from django.db import models
from django.urls import reverse


class Customer(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=13)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("customer_details", kwargs={"pk": self.pk})

    class Meta:
        ordering = ["last_name"]


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.vin}"
