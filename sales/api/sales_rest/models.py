from django.db import models
from django.urls import reverse


# Create your models here.
class Salesperson(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    employee_id = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("delete_salesperson", kwargs={"pk": self.pk})


# class Customer(models.Model):
class CustomerVO(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    # The following max_length is the max for ituE.164 normalized numbers
    phone_number = models.CharField(max_length=13)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("delete_customer", kwargs={"pk": self.pk})


class Sale(models.Model):
    automobile = models.ForeignKey(
        "AutomobileVO", on_delete=models.CASCADE, related_name="sales"
    )
    salesperson = models.ForeignKey(
        Salesperson, on_delete=models.CASCADE, related_name="sales"
    )
    customer = models.ForeignKey(
        CustomerVO, on_delete=models.CASCADE, related_name="sales"
    )
    # customer = models.ForeignKey(
    #     CustomerVO, on_delete=models.CASCADE, related_name="sales"
    # )
    price = models.FloatField()

    def __str__(self):
        return f"{self.salesperson.last_name}: {self.automobile.vin}"

    def get_api_url(self):
        return reverse("delete_sale", kwargs={"pk": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin}"
