from django.urls import path
from .views import (
    show_salespeople_list,
    delete_salesperson,
    show_sales_list,
    delete_sale,
)


urlpatterns = [
    path(
        "salespeople/",
        show_salespeople_list,
        name="show_salespeople_list",
    ),
    path(
        "salespeople/<int:pk>/",
        delete_salesperson,
        name="delete_salesperson",
    ),
    path(
        "sales/",
        show_sales_list,
        name="show_sales_list",
    ),
    path(
        "sales/<int:pk>/",
        delete_sale,
        name="delete_sale",
    ),
]
