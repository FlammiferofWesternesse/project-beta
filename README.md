# CarCar

Team:

* Nate - Services
* Andrew - Sales

## Design

## Service microservice

Much better than my practice project. I had the same approach to my back end, and I was able to get my front end responding. After that, I started putting everything together. I was focused on the appointments at the start so the "appts" is sort of interchangeable with "props" in some spots but for the most part itll be the same as far as functionality goes. I did a bit more front end work with getting certain things to show because doing it on the front end made a bit more sense to me in the moment.

## Sales microservice

This microservice has four models: Salesperson, Customer, Sale, and AutomobileVO. 
Salesperson has three fields: first_name, last_name, and employee_id, as well as __str__ and 
get_api_url methods.
Customer has four fields: first_name, last_name, address, and phone_number, as well as __str__ 
and get_api_url methods.
Sales has for fields: salesperson (foreignkey), customer (foreignkey), automobile (foreignkey 
to VO), and price, as well as __str__ and get_api_url methods.
AutomobileVO has two fields: vin and sold. AutomobileVOs are created via the poll/poller.py file which polls the inventory-api service for Autobomiles. poller.py imports the model declaration from poll/sales_rest/models instead of api/sales_rest/models. 
