import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()


from service_rest.models import AutomobileVO, CustomerVO


def get_customers():
    customer_endpoint = "http://project-beta-customers-api-1:8000/api/customers/"
    customer_response = requests.get(customer_endpoint)
    content = json.loads(customer_response.content)
    for customer in content["customers"]:
        CustomerVO.objects.update_or_create(
            defaults={
                "first_name": customer["first_name"],
                "last_name": customer["last_name"],
                "address": customer["address"],
                "phone_number": customer["phone_number"],
            }
        )


def get_automobiles():
    automobile_endpoint = (
        "http://project-beta-inventory-api-1:8000/api/automobiles/"
    )
    automobile_response = requests.get(automobile_endpoint)
    content = json.loads(automobile_response.content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            defaults={"sold": auto["sold"], "vin": auto["vin"]},
            vin=auto["vin"],
        )


def poll(repeat=True):
    while True:
        print("Service poller polling for data")
        try:
            get_customers()
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(15)


if __name__ == "__main__":
    poll()
