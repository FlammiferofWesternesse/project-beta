from django.urls import path
from .views import (
    show_techs_list,
    show_tech_details,
    show_appt_list,
    show_appt_details,

)

urlpatterns = [
    path("technicians/", show_techs_list, name="show_techs_list"),
    path("technicians/<int:pk>/", show_tech_details, name="show_tech_details"),
    path("appointments/", show_appt_list, name="show_appt_list"),
    path(
        "appointments/<int:pk>/", show_appt_details, name="show_appt_details"
    ),
]
