from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment, CustomerVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
        "sold",
    ]


class TechListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = CustomerVO
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechListEncoder(),
        "customer": CustomerEncoder(),
    }
