from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class CustomerVO(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    # The following max_length is the max for ituE.164 normalized numbers
    phone_number = models.CharField(max_length=13)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.IntegerField(null=True)

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("Tech_detail", kwargs={"pk": self.pk})


class Appointment(models.Model):
    STATUS_CHOICES = (
        ("created", "Created"),
        ("canceled", "Canceled"),
        ("finished", "Finished"),
    )

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default="created"
    )
    vin = models.CharField(max_length=17, unique=True)
    customer = models.ForeignKey(
        "CustomerVO",
        on_delete=models.CASCADE,
        related_name="appointments",
    )
    is_vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician, related_name="services", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.reason

    def get_api_url(self):
        return reverse("show_appt_details", kwargs={"pk": self.pk})
